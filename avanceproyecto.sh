#!/bin/bash

echo "__________________________________________________________"
echo "................Visualizador de proteinas................."
echo "__________________________________________________________"

echo "¿Desea visualizar algunos aminoacidos que conforman una proteina?"
echo "                responda "si" para acudir a ello                             "
read respuesta
respuesta=${respuesta^^}	

if [ $respuesta = "SI" ]; then
	echo "Las proteinas disponibles a visualizar sus aminoacidos son las siguientes:"
	r= grep -w Protein bd-pdb.txt
	echo $r
	   
elif [ "$#" -eq 0 ]; then
  echo "Error: La respuesta no corresponde a un "si", vuelva a intentarlo."
  exit      # originariamente "return 1"
fi

echo "ingrese el código de la proteina a visualizar"
read codigo

proteina=$(grep $codigo bd-pdb.txt | awk -F "," '{print $4}')
if [[ $proteina == '"Protein"' ]]; then
	wget https://files.rcsb.org/download/$codigo.pdb
	`awk 'BEGIN{FS=" ";OFS="\t"}($1=="ATOM"){print $1,$2,$3,$4,$5,$6}' $codigo.pdb > $codigo.txt`
	echo "su búsqueda ha finalizado"
	
		
else

	echo "EL codigo ingresado no pertenece a una proteina"
fi
echo $proteina
