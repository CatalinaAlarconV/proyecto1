#!/bin/bash

echo "____________________________________________________________________"
echo ".....................Visualizador de proteinas......................"
echo "____________________________________________________________________"

#se pide un código al usuario se pasa a mayúscula y se guardar en "$codigo"
echo "             ingrese el código de la proteina a visualizar          "
read codigo
codigo=${codigo^^}
#se revisa si existe en la base de datos, si no existe, informa del error y sale del programa.
if [[ ! -f bd-pdb.txt ]]; then
	echo "error: la base de datos no existe"
	exit 1
fi
#se extrae la información de la base de datos donde esté "$codigo" y se almacena en "$informacion", muestra la información al usuario.
informacion=$(grep -w $codigo bd-pdb.txt)
echo $informacion
#se revisa si $informacion contiene el texto "Protein",si el código corresponde a una proteína. si no es así, informa del error y saler.
if ! [[ $informacion =~ '"Protein"' ]]; then
	echo "EL codigo ingresado no pertenece a una proteina"
	exit 1
fi
#revisa si es que no se ha descargado la proteína. si es que no existe,se descarga. de lo contrario, informa que se saltará la descarga.
if [[ ! -f $codigo.pdb ]]; then
	echo "Se descargara $codigo"
	wget https://files.rcsb.org/download/$codigo.pdb
else
	echo "la proteina ya existe, saltando descarga"
fi
#se sacan todas las líneas que comiencen con ATOM del .pdb y se guardan en un .txt
echo "se va a procesar la proteina $codigo"
grep ^ATOM $codigo.pdb > $codigo.txt
#llama a awk para que corra filtrar.awk en el .txt que contiene todas las líneas que comienzan con ATOM.
awk -f filtrar.awk $codigo.txt 

#redirecciona la salida de awk a un archivo .dot







